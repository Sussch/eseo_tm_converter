#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# File converter for extracting ESEO telemetry from raw data.
#
# Indrek Synter 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import logging
import logging.handlers
import argparse
import struct
import os
import re

from collections import OrderedDict

from utility.log import init_logging
from utility.attrdict import AttrDict
from declarations import telemetry


class ESEOTelemetryConverter:
    """
    Converter to make ESEO telemetry human-readable.
    References:
        http://esamultimedia.esa.int/docs/edu/attachment_1_packetprotocol_rev1.pdf version 1.0, November 2018
    """

    # U frame modifiers, with the P/F flag masked out to 0.
    U_FRAME_MODIFIERS = {
        0x1B: ("SABME", "Cmd: Set Asynchronous Balanced Mode Extended"),
        0x0B: ("SABM", "Cmd: Set Asynchronous Balanced Mode"),
        0x10: ("DISC", "Cmd: Disconnect"),
        0x03: ("DM", "Res: Disconnected Mode"),
        0x18: ("UA", "Res: Unnumbered Acknowledge"),
        0x21: ("FRMR", "Res, not used"),
        0x00: ("UI", "Cmd, Res: Unnumbered Information, not used"),
        0x2B: ("XID", "Cmd, Res: Exchange Identification, not used"),
        0x38: ("TEST", "Cmd, Res: Test, not used")
    }

    def __init__(self):
        self.log = logging.getLogger("ETC")

        self.entries_i = []
        self.entries_s = []
        self.entries_u = []

    def decode_frame(self, binary):
        """Decode packet contents."""
        assert len(binary) >= 15

        # Read destination, source addresses and first control byte.
        address, control0,  = struct.unpack("<14sB", binary[:15])

        # Bit-shift addresses for readability.
        # ESEO address bytes are left-shifted by 1 ('E' = 0x8A, 'S' = 0xA6, etc.).
        address = "".join([chr(b >> 1) for b in address])
        # Determine frame type.
        frame_type_id = control0 & 0x03
        frame_type = ["I", "S", "I", "U"][frame_type_id]

        entry = OrderedDict([
            # ESEO address consists of a destination and source call-signs.
            ("addr_dest", address[0:7]),
            ("addr_src", address[7:]),
            ("ctl_type", frame_type),
            ("ctl_poll_final", 0),
        ])

        if frame_type == "I":
            control1, pid,  = struct.unpack("<BB", binary[15:17])
            entry["pid"] = "0x{:X}".format(pid)
            entry["ctl_seq_send"] = control0 >> 1
            entry["ctl_seq_recv"] = control1 >> 1
            entry["ctl_poll_final"] = control1 & 0x01

            return entry, binary[17:]
        elif frame_type == "S":
            control1,  = struct.unpack("<B", binary[15:16])
            entry["ctl_seq_recv"] = control1 >> 1
            entry["ctl_poll_final"] = control1 & 0x01
            supervisory_id = (control0 & 0x0C) >> 2
            entry["ctl_supervisory"] = ["RR", "RNR", "REJ", "SREJ"][supervisory_id]

            return entry, bytearray()
        else:
            entry["ctl_poll_final"] = control0 & 0x10
            modifier_id = (control0 & 0xEC) >> 2
            if modifier_id in self.U_FRAME_MODIFIERS:
                entry["ctl_modifiers"], entry["ctl_modifiers_desc"] = self.U_FRAME_MODIFIERS[modifier_id]
            else:
                entry["ctl_modifiers"], entry["ctl_modifiers_desc"] = ("0x{:X}".format(modifier_id), "Unknown")

            return entry, bytearray()

    def decode_i_frame_contents(self, binary):
        """Read frame ID, length and decode frame contents."""
        frame_id, frame_len,  = struct.unpack("<BB", binary[0:2])

        if frame_id in telemetry.DECLARATIONS:
            frame_name, declaration = telemetry.DECLARATIONS[frame_id]
            d = AttrDict.load_data_od_str(binary[2:], declaration)
            d["frame_id"] = frame_id
            d["frame_name"] = frame_name
            d["frame_len"] = frame_len

            return d
        return {"frame_id": frame_id, "frame_len": frame_len, "frame_name": "unknown"}

    def save_csvs(self, dir_path):
        """Save entries to CSV files, one for each frame type."""
        # I-frames
        for entry in self.entries_i:
            if "frame_id" in entry:
                path = os.path.join(dir_path, "eseo_hk{}_{}.csv".format(
                    entry["frame_id"], entry["frame_name"]))

                # Create the file with a header if the file does not exist yet.
                if not os.path.exists(path):
                    with open(path, "w+t") as fo:
                        fo.write(";".join(entry.keys()) + "\n")

                # Append rows with data
                with open(path, "a+t") as fo:
                    values = [str(v) for v in entry.values()]
                    fo.write(";".join(values) + "\n")

        # U-frames
        for entry in self.entries_u:
            path = os.path.join(dir_path, "eseo_u_frames.csv")

            # Create the file with a header if the file does not exist yet.
            if not os.path.exists(path):
                with open(path, "w+t") as fo:
                    fo.write(";".join(entry.keys()) + "\n")

            # Append rows with data
            with open(path, "a+t") as fo:
                values = [str(v) for v in entry.values()]
                fo.write(";".join(values) + "\n")

        # S-frames
        for entry in self.entries_s:
            path = os.path.join(dir_path, "eseo_s_frames.csv")

            # Create the file with a header if the file does not exist yet.
            if not os.path.exists(path):
                with open(path, "w+t") as fo:
                    fo.write(";".join(entry.keys()) + "\n")

            # Append rows with data
            with open(path, "a+t") as fo:
                values = [str(v) for v in entry.values()]
                fo.write(";".join(values) + "\n")

    def load_txt(self, path):
        """Load raw beacon data from a text file."""
        with open(path, "rt") as fi:
            lines = fi.readlines()

            # Support two types of formats:
            # 06/01/2019 19:45:34: 8AA68A9E40406092AE6888AA9861E0FFF0037AF20F34D...
            # 2019-01-06 21:11:21:
            # 8AA68A9E40406092AE6888AA9861F2FFF0067A0100C3...
            for i in range(len(lines)):
                line = lines[i]

                binary = None
                m = re.match(r"\s*([\d/-]+) ([\d:]+\d):\s*([a-fA-F\d]{14,})?\s*", line)
                if m:
                    # Everything on a single line?
                    if m.group(3):
                        binary = bytearray.fromhex(m.group(3))
                    # Or date-time on one line, and data on the next line?
                    elif i + 1 < len(lines):
                        line = lines[i+1]

                        mdata = re.match(r"^\s*([a-fA-F\d]{14,})\s*", line)
                        if mdata:
                            binary = bytearray.fromhex(mdata.group(1))

                    entry = {"date": "{} {}".format(m.group(1), m.group(2))}
                    frame_hdr, data = self.decode_frame(binary)
                    entry.update(frame_hdr)

                    # I-frame?
                    if frame_hdr["ctl_type"] == "I":
                        tm = self.decode_i_frame_contents(data)
                        entry.update(tm)

                        self.log.debug("{}: I 0x{:02X} ({}) [{}]".format(entry["date"], entry["frame_id"], entry["frame_name"], entry["frame_len"]))
                        self.entries_i.append(entry)
                    # U-frame?
                    elif frame_hdr["ctl_type"] == "U":
                        self.log.debug("{}: U {}".format(entry["date"], entry["ctl_modifiers_desc"]))
                        self.entries_u.append(entry)
                    # S-frame?
                    elif frame_hdr["ctl_type"] == "S":
                        self.log.debug("{}: S {}".format(entry["date"], entry["ctl_supervisory"]))
                        self.entries_s.append(entry)


def main():
    """Application main loop, which stops on Ctrl+C."""
    p = argparse.ArgumentParser()
    p.add_argument("path_in", help="Path to the text file containing the raw data.")
    p.add_argument("path_out", help="Path to the output directory.")
    p.add_argument("-l", "--log", dest="logfile", default="log_parser.log")
    p.add_argument("-v", "--verbose", dest="verbose", action="count",
                   help="Increase verbosity (specify multiple times for more)")

    arguments = p.parse_args()

    log = None
    try:
        log = init_logging(arguments, "ETC", arguments.logfile)
        log.info("ESEO Telemetry Converter started.")

        c = ESEOTelemetryConverter()
        c.load_txt(arguments.path_in)
        c.save_csvs(arguments.path_out)

        log.info("Closing..")

    except Exception as e:
        if log is not None:
            log.exception("Unhandled exception")
        else:
            print("Failed to initialize error logging: " + str(e))


if __name__ == '__main__':
    main()
