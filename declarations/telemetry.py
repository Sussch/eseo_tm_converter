# -*- coding: utf-8 -*-
#
# File converter for extracting ESEO telemetry from raw data.
#
# Indrek Synter 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

# Definitions of telemetry packets.
# Based on http://esamultimedia.esa.int/docs/edu/attachment_2_beacon_content.pdf Version 1.0, 27/11/201818:14:03
DECLARATIONS = {
    0x03: ("general", [
        {
            "key": "obd_mode", "desc": "Current OBDH platform mode",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "obd_active_task", "desc": "OBDH task currently executing",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "obd_equipment_status", "desc": "ON/OFF status of equipment/payload",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_cpu_error", "desc": "CPU fault errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_can_timeout_error", "desc": "CAN/CANopen interface timeout error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_wd_reset_count", "desc": "Number of watchdog resets",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "obd_rs422m_err_count", "desc": "TMTC USART interface main error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_rs422r_err_count", "desc": "TMTC USART interface redundant error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_error_count", "desc": "OBDH internal error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_tc_error_1", "desc": "Internal telecommand routing TC error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_tc_error_2", "desc": "Internal telecommand routing TC error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_status", "desc": "TMTC USART status defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_error", "desc": "TMTC USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_status", "desc": "MWM USART interface status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_error", "desc": "MWM USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_status", "desc": "OBDH status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {   # 1 - damping & safe, 2 - nominal
            "key": "acs_state", "desc": "AOCS mode of the state machine",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "acs_omega_p", "desc": "Roll angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "acs_omega_q", "desc": "Pitch angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "acs_omega_r", "desc": "Yaw angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "pm_current_bp1", "desc": "Current of the battery pack 1",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp2", "desc": "Current of the battery pack 2",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp3", "desc": "Current of the battery pack 3",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp4", "desc": "Current of the battery pack 4",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp5", "desc": "Current of the battery pack 5",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp6", "desc": "Current of the battery pack 6",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_voltage_mb", "desc": "Voltage of the Main Bus",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_safe_operating_mode", "desc": "Operating mode of the power system",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "pm_error_1", "desc": "Defined bit-per-bit (x- active PMU)",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttm_tx_status", "desc": "TMTC Main Transmitter STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttm_error", "desc": "TTM error condition defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "ttm_platform_fdir", "desc": "TTM FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttr_tx_status", "desc": "TMTC Redundant Transmitter STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttr_error", "desc": "TTR error condition defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "ttr_platform_fdir", "desc": "TTR FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ss_error_1", "desc": "SSM Error matrix defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ss_error_2", "desc": "SSM Error matrix defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ese_error", "desc": "ESE Fail matrix defined bit-per-bit below",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "mwr_error", "desc": "Wheel driver fault condition",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "mwm_status", "desc": "Status of reaction wheel",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "mmm_error", "desc": "MMM Fail code defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "mmr_error", "desc": "MMR Fail code defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "mtm_error", "desc": "MTM Fail code defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "mtr_error", "desc": "MTR Fail code defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "ttr_tx_status_2", "desc": "TMTC Redundant Transmitter STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttr_error_2", "desc": "TTR error condition defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
    ]),
    0x04: ("power", [
        {
            "key": "pm_voltage_sp1_string_1_2", "desc": "Voltage of a single string of the solar panel 1",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_voltage_sp1_string_3_4", "desc": "Voltage of a single string of the solar panel 1",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_voltage_sp2_string_1_2", "desc": "Voltage of a single string of the solar panel 2",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_voltage_sp2_string_3_4", "desc": "Voltage of a single string of the solar panel 2",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_voltage_sp3_string_1_2", "desc": "Voltage of a single string of the solar panel 3",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_voltage_sp3_string_3_4", "desc": "Voltage of a single string of the solar panel 3",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_1", "desc": "Current of shunt section 1",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_2", "desc": "Current of shunt section 2",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_3", "desc": "Current of shunt section 3",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_4", "desc": "Current of shunt section 4",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_5", "desc": "Current of shunt section 5",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_shunt_section_6", "desc": "Current of shunt section 6",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_temp_sp1_sens_1", "desc": "Temperature of the solar panel 1",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_sp2_sens_1", "desc": "Temperature of the solar panel 2",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_sp3_sens_1", "desc": "Temperature of the solar panel 3",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp1", "desc": "Current of the battery pack 1",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp2", "desc": "Current of the battery pack 2",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp3", "desc": "Current of the battery pack 3",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp4", "desc": "Current of the battery pack 4",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp5", "desc": "Current of the battery pack 5",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_current_bp6", "desc": "Current of the battery pack 6",
            "formula": "x", "unit": "mA",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp1_sens_1", "desc": "Temperature of the battery pack 1",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp2_sens_1", "desc": "Temperature of the battery pack 2",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp3_sens_1", "desc": "Temperature of the battery pack 3",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp4_sens_1", "desc": "Temperature of the battery pack 4",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp5_sens_1", "desc": "Temperature of the battery pack 5",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp_bp6_sens_1", "desc": "Temperature of the battery pack 6",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_voltage_mb", "desc": "Voltage of the Main Bus",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_safe_operating_mode", "desc": "Operating mode of the power system",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "pm_pdu_control", "desc": "PDU Control",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "pm_temp1", "desc": "Temperature of the power board - Sensor 1",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_temp2", "desc": "Temperature of the power board - Sensor 2",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pm_obdh_main_current", "desc": "Current drawn by OBDH main",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_rx_main_current", "desc": "Current drawn by the main RX",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_tx_main_current", "desc": "Current drawn by the main TX",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_ss_main_current", "desc": "Current drawn by the main Sun Sensor",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mm_main_current", "desc": "Current drawn by the main Magnetometer",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mw_main_current", "desc": "Current drawn by the main Momentum Wheel",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mt_main_current", "desc": "Current drawn by the main Magneto Torquer",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mps_current", "desc": "Current drawn by MPS",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_tritel_current", "desc": "Current drawn by TRITEL",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_hstx_current", "desc": "Current drawn by HSTX",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_gps_current", "desc": "Current drawn by GPS",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mps_valve_m_current", "desc": "Current drawn by the MPS Start Valve Main",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_dom_1_current", "desc": "Current drawn by the DOM actuator 1",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_obdh_red_current", "desc": "Current drawn by OBDH redundant",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_rx_red_current", "desc": "Current drawn by the redundant RX",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_tx_red_current", "desc": "Current drawn by the redundant TX",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_ss_red_current", "desc": "Current drawn by the redundant Sun Sensor",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mm_red_current", "desc": "Current drawn by the redundant Magnetometer",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mw_red_current", "desc": "Current drawn by the redundant Momentum Wheel",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_mt_red_current", "desc": "Current drawn by the redundant Magneto Torquer",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_es_current", "desc": "Current drawn by ES",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_cam_current", "desc": "Current drawn by Cameras",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_amsat_current", "desc": "Current drawn by the AMSAT payload",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_lmp_current", "desc": "Current drawn by LMP",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "pm_eq_pl_status", "desc": "ON-OFF equipment status (1-ON, 0-OFF)",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "pm_error_1", "desc": "Defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "pm_error_2", "desc": "Defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
    ]),
    0x05: ("obdh", [
        {
            "key": "obd_mode", "desc": "Current OBDH platform mode",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "obd_old_mode", "desc": "Previous OBDH platform mode",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "obd_active_task", "desc": "OBDH task currently executing",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "obd_equipment_status", "desc": "ON/OFF status of equipment/payload",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_equipment_health", "desc": "Equipment health (1 healthy, 0 faulty)",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_cpu_error", "desc": "CPU fault errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_can_status", "desc": "CAN status defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_plcan_m_error", "desc": "CAN/CANopen platform main interface errors",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_plcan_r_error", "desc": "CAN/CANopen platform redundant interface errors",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_pycan_m_error", "desc": "CAN/CANopen payload main interface errors",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_pycan_r_error", "desc": "CAN/CANopen payload redundant interface errors",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_can_timeout_error", "desc": "CAN/CANopen interface timeout error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_hk_status", "desc": "HK data request status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_power_time", "desc": "Seconds past OBDH activation",
            "formula": "x", "unit": "s",
            "format_string": "Q"
        },
        {
            "key": "obd_mode_transition", "desc": "Seconds past last mode transition",
            "formula": "x", "unit": "s",
            "format_string": "Q"
        },
        {
            "key": "obd_wd_reset_count", "desc": "Number of watchdog resets",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "obd_temp1_pdu1", "desc": "Temperature of the PDU module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp2_bat1", "desc": "Temperature of the battery module 1",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp3_pmb", "desc": "Temperature of PMB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp4_hpa2", "desc": "Temperature of HPA2",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp8_hpa1", "desc": "Temperature of HPA1",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp10_tnk", "desc": "Temperature of the propellant tank",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp11_bat2", "desc": "Temperature of the battery module 2",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp12_mwm", "desc": "Temperature of the main momentum wheel module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp13_mwr", "desc": "Temperature of the redundant momentum wheel module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp14_mmm", "desc": "Temperature of the main magnetometer module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_temp15_mmr", "desc": "Temperature of the redundant magnetometer module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "obd_rs422m_err_count", "desc": "TMTC USART interface main error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_rs422r_err_count", "desc": "TMTC USART interface redundant error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_hk_error", "desc": "HK data request error",
            "formula": "x", "unit": "dec",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_status", "desc": "TMTC USART status defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_error", "desc": "TMTC USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_status", "desc": "MWM USART interface status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_error", "desc": "MWM USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_status", "desc": "OBDH status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_error_1", "desc": "OBDH internal error 1",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_temp_error_1", "desc": "OBDH temperatures error 1",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "obd_error_2", "desc": "OBDH internal error 2",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_temp_error_2", "desc": "OBDH temperatures error 2",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
    ]),
    0x06: ("aocs", [
        {   # 1 - damping & safe, 2 - nominal
            "key": "acs_state", "desc": "AOCS mode of the state machine",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "acs_sun_mode", "desc": "ACS sun-eclipse evaluation mode",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "acs_err", "desc": "ACS error table",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "acs_attitude_q1", "desc": "First component of AOCS quaternion vector",
            "formula": "x", "unit": "",
            "format_string": "f"
        },
        {
            "key": "acs_attitude_q2", "desc": "Second component of AOCS quaternion vector",
            "formula": "x", "unit": "",
            "format_string": "f"
        },
        {
            "key": "acs_attitude_q3", "desc": "Third component of AOCS quaternion vector",
            "formula": "x", "unit": "",
            "format_string": "f"
        },
        {
            "key": "acs_attitude_q4", "desc": "Scalar component of AOCS quaternion vector",
            "formula": "x", "unit": "",
            "format_string": "f"
        },
        {
            "key": "acs_omega_p", "desc": "Roll angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "acs_omega_q", "desc": "Pitch angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "acs_omega_r", "desc": "Yaw angular rate",
            "formula": "x", "unit": "deg/s",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_x", "desc": "SGP4 x component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_y", "desc": "SGP4 y component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_z", "desc": "SGP4 z component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_vx", "desc": "SGP4 Vx component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_vy", "desc": "SGP4 Vy component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_orbit_vz", "desc": "SGP4 Vz component",
            "formula": "x", "unit": "km",
            "format_string": "f"
        },
        {
            "key": "acs_state_transition", "desc": "Seconds past from the last transition",
            "formula": "x", "unit": "s",
            "format_string": "Q"
        },
        {
            "key": "acs_fdir_mps_time_err", "desc": "If a maneuver is aborted the variable returns the time",
            "formula": "x", "unit": "s",
            "format_string": "f"
        },
        {
            "key": "pm_spin_rate", "desc": "Spin rate calculated by PMM through Coarse Sun Sensors",
            "formula": "x", "unit": "dec",
            "format_string": "I"
        },
        {
            "key": "ssm_uc_pcb_temp", "desc": "Main Sun Sensor: PCB temperature (TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssm_adc1_pcb_temp", "desc": "Main Sun Sensor: ADC1 internal temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssm_adc2_pcb_temp", "desc": "Main Sun Sensor: ADC2 internal temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssm_topcase_temp", "desc": "Main Sun Sensor: Top side of case (external TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssm_sidecase_temp", "desc": "Main Sun Sensor: Lateral side of case (external TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssr_uc_pcb_temp", "desc": "Redundant Sun Sensor: PCB temperature (TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssr_adc1_pcb_temp", "desc": "Redundant Sun Sensor: ADC1 internal temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssr_adc2_pcb_temp", "desc": "Redundant Sun Sensor: ADC2 internal temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ss_dcdc_temp", "desc": "Temperature of the DC-DC converter for Sun Sensors",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ssr_sidecase_temp", "desc": "Redundant Sun Sensor: Lateral side of case (external TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ese_uc_pcb_temp", "desc": "ESE: PCB temperature (TMP36)",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ese_tau_temp", "desc": "ESE: Temperature of the TAU camera",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mwr_temp", "desc": "Temperature of the Redundant Momentum Wheel at TRP",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mwr_if_temp1", "desc": "Temperature of the Redundant Momentum Wheel Interface Board at TRP",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mwm_temperature_1", "desc": "Temperature of the Main Momentum Wheel Motor",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mwm_temperature_2", "desc": "Temperature of the Main Momentum Wheel Power module",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mwm_temperature_3", "desc": "Temperature of the Main Momentum Wheel Controller",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mps_hpt01", "desc": "Tank pressure at the High Pressure Transducer",
            "formula": "x", "unit": "kPa",
            "format_string": "H"
        },
        {
            "key": "mps_lpt01", "desc": "Pressure downstream the pressure regulator",
            "formula": "x", "unit": "kPa",
            "format_string": "H"
        },
        {
            "key": "mps_pvtt01", "desc": "Temperature of the Vessel Pressure Transducer",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mm_dc_dc_temp", "desc": "Temperature of the DC-DC on the Main Magnetometer PCB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mr_dc_dc_temp", "desc": "Temperature of the DC-DC on the Redundant Magnetometer PCB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mtm_temp1", "desc": "Temperature of the Main Magnetic Torquer PCB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "mtr_temp1", "desc": "Temperature of the Redundant Magnetic Torquer PCB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
    ]),
    0x07: ("fdir", [
        {
            "key": "obd_plcan_m_txerr_count", "desc": "CAN/CANopen platform interface main Tx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_plcan_m_rxerr_count", "desc": "CAN/CANopen platform interface main Rx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_plcan_r_txerr_count", "desc": "CAN/CANopen platform interface redundant Tx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_plcan_r_rxerr_count", "desc": "CAN/CANopen platform interface redundant Rx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_pycan_m_txerr_count", "desc": "CAN/CANopen payload interface main Tx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_pycan_m_rxerr_count", "desc": "CAN/CANopen payload interface main Rx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_pycan_r_txerr_count", "desc": "CAN/CANopen payload interface redundant Tx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_pycan_r_rxerr_count", "desc": "CAN/CANopen payload interface redundant Rx error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_edac_error_count", "desc": "Number of EDAC errors since OBDH reset",
            "formula": "x", "unit": "dec",
            "format_string": "I"
        },
        {
            "key": "obd_rs422m_err_count", "desc": "TMTC USART interface main error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_rs422r_err_count", "desc": "TMTC USART interface redundant error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_error_count", "desc": "OBDH internal error counter",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "obd_hk_error", "desc": "HK data request error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_tc_error_1", "desc": "Internal telecommand routing TC error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_tc_error_2", "desc": "Internal telecommand routing TC error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_status", "desc": "TMTC USART status defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs422_error", "desc": "TMTC USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_status", "desc": "MWM USART interface status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_rs485_error", "desc": "MWM USART interface errors, defined bit per bit",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_error", "desc": "OBDH internal error",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "obd_temp_error", "desc": "OBDH temperatures error",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "acs_err", "desc": "AOCS Error table",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "acs_fdir_mps_time_err", "desc": "If a maneuver is aborted the variable returns the time",
            "formula": "x", "unit": "s",
            "format_string": "f"
        },
        {
            "key": "pm_voltage_mb", "desc": "Voltage of the Main Bus",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "pm_safe_operating_mode", "desc": "Operating mode of the power system",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "pm_eq_pl_status", "desc": "ON-OFF equipment status (1-ON, 0-OFF)",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "pm_undervoltage_status", "desc": "Undervoltage status (1-undervoltage, 0-normal)",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttm_tx_status", "desc": "TMTC Main Transmitter STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttm_tx_current1", "desc": "TMTC Main charge pump current of the TX section",
            "formula": "x", "unit": "uA",
            "format_string": "B"
        },
        {
            "key": "ttm_rx_status", "desc": "TMTC Main Receiver STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttm_rx_current1", "desc": "TMTC Main charge pump current of the RX section",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "ttm_rx_rssi", "desc": "TMTC Main RSSI register of the RX section",
            "formula": "x", "unit": "dBm",
            "format_string": "b"
        },
        {
            "key": "ttm_error", "desc": "TTM error condition defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "ttm_temp1", "desc": "Temperature of the DC/DC section monitored on board",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ttm_temp2", "desc": "Temperature of the RF front-end monitored on board",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ttm_rx_afc", "desc": "TMTC Main frequency deviation from IF of the RX section",
            "formula": "0.0625*x", "unit": "Hz",
            "format_string": "b"
        },
        {
            "key": "ttm_platform_fdir", "desc": "TTM FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttr_tx_status", "desc": "TMTC Redundant Transmitter STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttr_tx_current1", "desc": "TMTC Redundant charge pump current of the TX section",
            "formula": "x", "unit": "uA",
            "format_string": "B"
        },
        {
            "key": "ttr_rx_status", "desc": "TMTC Redundant Receiver STATUS condition",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "ttr_rx_current1", "desc": "TMTC Redundant charge pump current of the RX section",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "ttr_rx_rssi", "desc": "TMTC Redundant RSSI register of the RX section",
            "formula": "x", "unit": "dBm",
            "format_string": "b"
        },
        {
            "key": "ttr_error", "desc": "TTR error condition defined bit-per-bit",
            "formula": "x", "unit": "hex",
            "format_string": "H"
        },
        {
            "key": "ttr_temp1", "desc": "Temperature of the DC/DC section monitored on board",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ttr_temp2", "desc": "Temperature of the RF front-end monitored on board",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ttr_rx_afc", "desc": "TMTC Redundant frequency deviation from IF of the RX section",
            "formula": "0.0625*x", "unit": "Hz",
            "format_string": "b"
        },
        {   # TBD: Why so many FDIR values for TTR?
            "key": "ttr_platform_fdir1", "desc": "TTR FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttr_platform_fdir2", "desc": "TTR FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ttr_platform_fdir3", "desc": "TTR FDIR platform status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
    ]),
    0x08: ("payload", [
        {
            "key": "tri_tmpx", "desc": "TRITEL: X-axis detector temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpy", "desc": "TRITEL: Y-axis detector temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpz", "desc": "TRITEL: Z-axis detector temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmppsu", "desc": "TRITEL: Power Supply Unit temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpcpu", "desc": "TRITEL: Central Processing Unit temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpadcx", "desc": "TRITEL: X-axis ADC temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpadcy", "desc": "TRITEL: Y-axis ADC temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_tmpadcz", "desc": "TRITEL: Z-axis ADC temperature",
            "formula": "0.5*x-40", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "tri_uinput", "desc": "TRITEL: Input voltage",
            "formula": "150*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "tri_iinput", "desc": "TRITEL: Input current",
            "formula": "2*x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "tri_60v", "desc": "TRITEL: Internal 60 V",
            "formula": "300*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "tri_5v", "desc": "TRITEL: Internal 5 V",
            "formula": "30*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "tri_3_3v", "desc": "TRITEL: Internal 3.3 V",
            "formula": "20*x", "unit": "mV",
            "format_string": "B"
        },
        {   # TBD: why not negative?
            "key": "tri_neg10v", "desc": "TRITEL: Internal -10 V",
            "formula": "-100*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "tri_6_5v", "desc": "TRITEL: Internal 6.5 V",
            "formula": "50*x", "unit": "mV",
            "format_string": "B"
        },
        {   # TBD: why not negative?
            "key": "tri_neg6_5v", "desc": "TRITEL: Internal -6.5 V",
            "formula": "-50*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "tri_mode", "desc": "TRITEL: Measurement mode",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "tri_freq", "desc": "TRITEL: Impulse generator frequency",
            "formula": "x", "unit": "Hz",
            "format_string": "B"
        },
        {
            "key": "tri_error", "desc": "TRITEL: HK parameter warnings / errors",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "tri_eeprom", "desc": "TRITEL: EEPROM corruption",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "lmp_tt_psu", "desc": "LMP: PSU temperature",
            "formula": "x", "unit": "*C",
            "format_string": "b"
        },
        {
            "key": "lmp_vt_12", "desc": "LMP: +12 V Power Supply Voltage",
            "formula": "0.078*x", "unit": "V",
            "format_string": "B"
        },
        {
            "key": "lmp_vt_neg12", "desc": "LMP: -12 V Power Supply Voltage",
            "formula": "-0.078*x", "unit": "V",
            "format_string": "B"
        },
        {
            "key": "lmp_vt_5", "desc": "LMP: +5 V Power Supply Voltage",
            "formula": "0.029*x", "unit": "V",
            "format_string": "B"
        },
        {   # TBD: why -0.028 not -0.029 like lmp_vt_5
            "key": "lmp_vt_neg5", "desc": "LMP: -5 V Power Supply Voltage",
            "formula": "-0.028*x", "unit": "V",
            "format_string": "B"
        },
        {
            "key": "lmp_ct_dig", "desc": "LMP: +3.3 V Supply Current",
            "formula": "1.259*x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "lmp_vt_dig", "desc": "LMP: +3.3 V Supply Voltage",
            "formula": "0.02*x", "unit": "V",
            "format_string": "B"
        },
        {
            "key": "lmp_mem", "desc": "LMP: Usage of external flash memory",
            "formula": "4096*x", "unit": "B",
            "format_string": "B"
        },
        {
            "key": "lmp_ofst", "desc": "LMP: Offset voltage of the ADC signal conditioning circuit",
            "formula": "4.88*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "lmp_sw1", "desc": "LMP: Status 1",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "lmp_sw2", "desc": "LMP: Status 2",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "lmp_sw3", "desc": "LMP: Status 3",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {
            "key": "pcam_mcur_curr", "desc": "PCAM: MCU + SRAM current consumption",
            "formula": "x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "pcam_img_curr", "desc": "PCAM: Image sensor current consumption",
            "formula": "x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "pcam_mcu_temp", "desc": "PCAM: MCU temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pcam_img_temp", "desc": "PCAM: Image sensor temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "pcam_dcdc_temp", "desc": "PCAM: DC-DC converter temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "scam_mcu_curr", "desc": "SCAM: MCU current consumption",
            "formula": "x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "scam_img_curr", "desc": "SCAM: Image sensor current consumption",
            "formula": "x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "scam_ram_curr", "desc": "SCAM: SDRAM current consumption",
            "formula": "x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "scam_mcu_temp", "desc": "SCAM: MCU temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "scam_img_temp", "desc": "SCAM: Image sensor temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "scam_sdr1_temp", "desc": "SCAM: SDRAM1 temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "scam_sdr2_temp", "desc": "SCAM: SDRAM2 temperature",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "ams_obc_p_up", "desc": "AMSAT: Uplink packet counter",
            "formula": "x", "unit": "dec",
            "format_string": "I"
        },
        {
            "key": "ams_obc_p_up_dropped", "desc": "AMSAT: Failed uplink packet counter",
            "formula": "x", "unit": "dec",
            "format_string": "I"
        },
        {
            "key": "ams_obc_mem_stat_1", "desc": "AMSAT: RAM Read/Write/ECC Checks",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ams_obc_mem_stat_2", "desc": "AMSAT: FLASH Read/Write/ECC Checks",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "ams_eps_dcdc_t", "desc": "AMSAT: EPS DC-DC converter temperature",
            "formula": "x", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "ams_vhf_fm_pa_t", "desc": "AMSAT: FM power amplifier temperature",
            "formula": "x", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "ams_vhf_bpsk_pa_t", "desc": "AMSAT: BPSK power amplifier temperature",
            "formula": "x", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "stx_vol_1", "desc": "HSTX: iRF Power Amplifier DC supply voltage",
            "formula": "50*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "stx_vol_2", "desc": "HSTX: Digital board DC supply voltage",
            "formula": "20*x", "unit": "mV",
            "format_string": "B"
        },
        {
            "key": "stx_cur_1", "desc": "HSTX: RF Power Amplifier DC supply current",
            "formula": "10*x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "stx_cur_2", "desc": "HSTX: Digital board DC supply current",
            "formula": "10*x", "unit": "mA",
            "format_string": "B"
        },
        {
            "key": "stx_temp_1", "desc": "HSTX: FPGA temperature",
            "formula": "0.5*x-43", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "stx_temp_2", "desc": "HSTX: RF modulator LIME RF transceiver temperature",
            "formula": "0.5*x-43", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "stx_temp_3", "desc": "HSTX: Temperature of the microwave Power Amplifier DC supply",
            "formula": "0.5*x-43", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "stx_temp_4", "desc": "HSTX: Temperature of the digital supply",
            "formula": "0.5*x-43", "unit": "*C",
            "format_string": "B"
        },
        {
            "key": "stx_stat", "desc": "HSTX: Status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "stx_com", "desc": "HSTX: Communication status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "stx_mem", "desc": "HSTX: Memory status",
            "formula": "x", "unit": "hex",
            "format_string": "I"
        },
        {
            "key": "gps_current_3v3", "desc": "GPS: Current absorption on 3.3 V power bus",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "gps_current_5v", "desc": "GPS: Current absorption on 5 V power bus",
            "formula": "x", "unit": "mA",
            "format_string": "H"
        },
        {
            "key": "gps_week", "desc": "GPS: Week",
            "formula": "x", "unit": "dec",
            "format_string": "H"
        },
        {
            "key": "gps_temperature_1", "desc": "GPS: Temperature of the PCB",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "gps_temperature_2", "desc": "GPS: Temperature of the chassis",
            "formula": "0.1*x", "unit": "*C",
            "format_string": "h"
        },
        {
            "key": "gps_frend_m_volt", "desc": "GPS: Main COTS front-end input voltage",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "gps_frend_r_volt", "desc": "GPS: Redundant COTS front-end input voltage",
            "formula": "x", "unit": "mV",
            "format_string": "H"
        },
        {
            "key": "gps_seconds_of_week", "desc": "GPS: Seconds of the GPS week accurate to the millisecond",
            "formula": "x", "unit": "ms",
            "format_string": "I"
        },
        {
            "key": "ade_in_estimator_on", "desc": "ADE: Indicator of estimators",
            "formula": "x", "unit": "hex",
            "format_string": "B"
        },
        {   # 0 - ESEO AOCS, 1 - ADE MEKF
            "key": "ade_in_omega", "desc": "ADE: Angular rate estimate used",
            "formula": "x", "unit": "dec",
            "format_string": "B"
        },
        {
            "key": "ade_oprq_q_1", "desc": "ADE: First element of the quaternion estimated from the filtered K matrix",
            "formula": "x", "unit": "rad",
            "format_string": "f"
        },
        {
            "key": "ade_oprq_q_2", "desc": "ADE: Second element of the quaternion estimated from the filtered K matrix",
            "formula": "x", "unit": "rad",
            "format_string": "f"
        },
        {
            "key": "ade_oprq_q_3", "desc": "ADE: Third element of the quaternion estimated from the filtered K matrix",
            "formula": "x", "unit": "rad",
            "format_string": "f"
        },
    ]),
}
