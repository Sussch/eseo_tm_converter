# -*- coding: utf-8 -*-
#
# File converter for extracting ESEO telemetry from raw data.
#
# Indrek Synter 2018
#
# The contents of this file are subject to the MIT License.
# The license is available at http://opensource.org/licenses/MIT

import struct
from collections import OrderedDict


class AttrDict(dict):
    """Dictionary with parameters accessible as class member variables."""
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

    @staticmethod
    def load_data(data, definitions):
        """Convert binary data into an object, based on field definitions."""
        data = bytearray(data)
        format_string = ''
        keys = []
        for d in definitions:
            # Variable-length strings are only supported as the last parameter.
            if d['format_string'] == 's':
                size = struct.calcsize('<{}'.format(format_string))
                format_string += str(len(data) - size)
            format_string += d['format_string']
            keys.append(d['key'])
        values = struct.unpack('<{}'.format(format_string), data)
        return zip(keys, values)

    @staticmethod
    def load_data_od_str(data, definitions):
        od = OrderedDict(AttrDict.load_data(data, definitions))

        for d in definitions:
            key = d['key']
            val = od[key]
            val_scaled = eval(d['formula'], {}, {'x': val})

            if d['unit'] == 'hex':
                val_str = '0x{:X}'.format(val_scaled)
            elif d['unit'] == 'bin':
                val_str = '{:b}'.format(val_scaled)
            else:
                val_str = str(val_scaled)
            od[key] = val_str
        return od
